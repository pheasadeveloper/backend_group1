# Basic MySQL

## Intro
MySQL is an open-source cross platform RDBMS (Relational Database Management System) that was developed by ORACLE Corporation, first released back in 1995.  

## RDBMS
RDBMS (Relational Database Management System) is a software that use to manage relational database and it used SQL Query to manage and manipulate data in the database.  
Some of the modern RDBMS are MySQL, Microsoft SQL Server, Oracle etc.  
## SQL
SQL (Structured Query Language) is the standard language for storing and processing information in a relational database.  
SQL keywords are not case sensitive: select and SELECT is the same but it is recommended to write SQL key word in upper-case.  

Some of The Most Important SQL Commands    
- SELECT : extracts data FROM a database
- UPDATE : updates data in a database
- DELETE : deletes data FROM a database
- INSERT INTO : inserts new data into a database
- CREATE DATABASE : creates a new database
- ALTER DATABASE : modifies a database
- CREATE TABLE : creates a new table
- ALTER TABLE : modifies a table
- DROP TABLE : deletes a table
- CREATE INDEX : creates an INDEX (search key)
- DROP INDEX : deletes an index

## SELECT Statement
SELECT Statement is used to select data FROM a database.  
The data returned is stored in a result TABLE called the result-set.  
Syntax: 

``` sql
SELECT column1, column2, ...
FROM table_name;
```

column1, column2, … are the field names of the TABLE you want to select data FROM.
table_name is the name of the database TABLE you want to select data FROM.
If you want to select all fields in the TABLE you can use below syntax:  
``` sql
SELECT * FROM table_name; 
```
The “*” specify all fields in the table.  
Example: This is an example TABLE call pets  

|    id    | name     | owner  | species | sex  | birth      | death      |
|----------|----------|--------|---------|------|------------|------------|
|    1     | Fluffy   | Harold | cat     | f    | 1993-02-04 | NULL       |
|    2     | Claws    | Gwen   | cat     | m    | 1994-03-17 | NULL       |
|    3     | Buffy    | Harold | dog     | f    | 1989-05-13 | NULL       |
|    4     | Fang     | Benny  | dog     | m    | 1990-08-27 | NULL       |
|    5     | Bowser   | Diane  | dog     | m    | 1979-08-31 | 1995-07-29 |
|    6     | Chirpy   | Gwen   | bird    | f    | 1998-09-11 | NULL       |
|    7     | Whistler | Gwen   | bird    | NULL | 1997-12-09 | NULL       |
|    8     | Slim     | Benny  | snake   | m    | 1996-04-29 | NULL       |
|    9     | Puffball | Diane  | hamster | f    | 1999-03-30 | NULL       |

Query: 
```sql
SELECT name, species, sex FROM pets;
```  
Result:   
| name     |species | sex  |
|----------|--------|------|
| Fluffy   | cat    | f    |
| Claws    | cat    | m    |
| Buffy    | dog    | f    |
| Fang     | dog    | m    |
| Bowser   | dog    | m    |
| Chirpy   | bird   | f    |
| Whistler | bird   | NULL |
| Slim     | snake  | m    |
| Puffball | hamster| f    |

## SELECT DISTINCT Statement  
**SELECT DISTINCT** Statement is used to return only distinct (different) values meaning this statement will only return the value only once even there are duplicated value in the table.  
Syntax:  
```sql
SELECT DISTINCT column1, column2, ...
FROM table_name;
```
Example: this is an example TABLE call pets  
|    id    | name     | owner  | species | sex  | birth      | death      |
|----------|----------|--------|---------|------|------------|------------|
|    1     | Fluffy   | Harold | cat     | f    | 1993-02-04 | NULL       |
|    2     | Claws    | Gwen   | cat     | m    | 1994-03-17 | NULL       |
|    3     | Buffy    | Harold | dog     | f    | 1989-05-13 | NULL       |
|    4     | Fang     | Benny  | dog     | m    | 1990-08-27 | NULL       |
|    5     | Bowser   | Diane  | dog     | m    | 1979-08-31 | 1995-07-29 |
|    6     | Chirpy   | Gwen   | bird    | f    | 1998-09-11 | NULL       |
|    7     | Whistler | Gwen   | bird    | NULL | 1997-12-09 | NULL       |
|    8     | Slim     | Benny  | snake   | m    | 1996-04-29 | NULL       |
|    9     | Puffball | Diane  | hamster | f    | 1999-03-30 | NULL       |

Query:
```sql
SELECT DISTINCT species FROM pets;
```
Result:
| species |
|---------|
| cat     |
| dog     |
| bird    |
| snake   |
| hamster |

## WHERE Clause
WHERE clause is use to filter records. It is used to extract only those records that fulfill a specific condition.
Syntax:
```sql
SELECT column1, column2, ...
FROM table_name
WHERE condition;
```
Example: This is an example TABLE call pets  
|    id    | name     | owner  | species | sex  | birth      | death      |
|----------|----------|--------|---------|------|------------|------------|
|    1     | Fluffy   | Harold | cat     | f    | 1993-02-04 | NULL       |
|    2     | Claws    | Gwen   | cat     | m    | 1994-03-17 | NULL       |
|    3     | Buffy    | Harold | dog     | f    | 1989-05-13 | NULL       |
|    4     | Fang     | Benny  | dog     | m    | 1990-08-27 | NULL       |
|    5     | Bowser   | Diane  | dog     | m    | 1979-08-31 | 1995-07-29 |
|    6     | Chirpy   | Gwen   | bird    | f    | 1998-09-11 | NULL       |
|    7     | Whistler | Gwen   | bird    | NULL | 1997-12-09 | NULL       |
|    8     | Slim     | Benny  | snake   | m    | 1996-04-29 | NULL       |
|    9     | Puffball | Diane  | hamster | f    | 1999-03-30 | NULL       |  

Query: 
```sql
SELECT * FROM pets WHERE species=’cat’;
```
Result: 
|    id    | name     | owner  | species | sex  | birth      | death      |
|----------|----------|--------|---------|------|------------|------------|
|    1     | Fluffy   | Harold | cat     | f    | 1993-02-04 | NULL       |
|    2     | Claws    | Gwen   | cat     | m    | 1994-03-17 | NULL       |

WHERE clause is not only use in select statement, it is also use in other statement like UPDATE, DELETE, etc. as well.  
These are some of the operator that can be use with WHERE clause.  

|  Operator  |  Description  |
|------------|---------------|
|\=	       | Equal  |
|\>	       |Greater than|  
|\<	       | Less than  |
|\>=	   |     Greater than or equal|  
|\<=	   |     Less than or equal  |
|\<>	   |     Not equal. Note: In some versions of SQL this operator may be written as !=|  
|BETWEEN	|    BETWEEN a certain range  |
|LIKE	    |Search for a pattern  |
|IN	        |To specify multiple possible values for a column|    

## AND, OR and NOT Operator
The WHERE clause can be use with AND, OR and NOT operator.  
### AND operator will return true if all the conditions are true.  
Syntax: 
```sql
SELECT column1, column2, ...
FROM table_name
WHERE condition1 AND condition2 AND condition3 ...;
```
Example: This is an example TABLE call pets.  
|    id    | name     | owner  | species | sex  | birth      | death      |
|----------|----------|--------|---------|------|------------|------------|
|    1     | Fluffy   | Harold | cat     | f    | 1993-02-04 | NULL       |
|    2     | Claws    | Gwen   | cat     | m    | 1994-03-17 | NULL       |
|    3     | Buffy    | Harold | dog     | f    | 1989-05-13 | NULL       |
|    4     | Fang     | Benny  | dog     | m    | 1990-08-27 | NULL       |
|    5     | Bowser   | Diane  | dog     | m    | 1979-08-31 | 1995-07-29 |
|    6     | Chirpy   | Gwen   | bird    | f    | 1998-09-11 | NULL       |
|    7     | Whistler | Gwen   | bird    | NULL | 1997-12-09 | NULL       |
|    8     | Slim     | Benny  | snake   | m    | 1996-04-29 | NULL       |
|    9     | Puffball | Diane  | hamster | f    | 1999-03-30 | NULL       |

Query: 
```sql
SELECT * FROM pets WHERE species=’cat’ AND sex=’m’;
```
Result: 
|    id    | name     | owner  | species | sex  | birth      | death      |
|----------|----------|--------|---------|------|------------|------------|
|    2     | Claws    | Gwen   | cat     | m    | 1994-03-17 | NULL       |

### OR operator will return true if any of the conditions is true.  
Syntax: 
```sql
SELECT column1, column2, ...
FROM table_name
WHERE condition1 OR condition2 OR condition3 ...;
```
Example: This is an example TABLE call pets.  
|    id    | name     | owner  | species | sex  | birth      | death      |
|----------|----------|--------|---------|------|------------|------------|
|    1     | Fluffy   | Harold | cat     | f    | 1993-02-04 | NULL       |
|    2     | Claws    | Gwen   | cat     | m    | 1994-03-17 | NULL       |
|    3     | Buffy    | Harold | dog     | f    | 1989-05-13 | NULL       |
|    4     | Fang     | Benny  | dog     | m    | 1990-08-27 | NULL       |
|    5     | Bowser   | Diane  | dog     | m    | 1979-08-31 | 1995-07-29 |
|    6     | Chirpy   | Gwen   | bird    | f    | 1998-09-11 | NULL       |
|    7     | Whistler | Gwen   | bird    | NULL | 1997-12-09 | NULL       |
|    8     | Slim     | Benny  | snake   | m    | 1996-04-29 | NULL       |
|    9     | Puffball | Diane  | hamster | f    | 1999-03-30 | NULL       |
Query: 
```sql
SELECT * FROM pets WHERE species=’cat’ OR species=’bird’;
```
Result:   
|    id    | name     | owner  | species | sex  | birth      | death      |
|----------|----------|--------|---------|------|------------|------------|
|    1     | Fluffy   | Harold | cat     | f    | 1993-02-04 | NULL       |
|    2     | Claws    | Gwen   | cat     | m    | 1994-03-17 | NULL       |
|    6     | Chirpy   | Gwen   | bird    | f    | 1998-09-11 | NULL       |
|    7     | Whistler | Gwen   | bird    | NULL | 1997-12-09 | NULL       |

### NOT operator will return true if the condition is not true.
Syntax:  
```sql
SELECT column1, column2, ...
FROM table_name
WHERE NOT condition;
```
Example: This is an example TABLE call pets.  
|    id    | name     | owner  | species | sex  | birth      | death      |
|----------|----------|--------|---------|------|------------|------------|
|    1     | Fluffy   | Harold | cat     | f    | 1993-02-04 | NULL       |
|    2     | Claws    | Gwen   | cat     | m    | 1994-03-17 | NULL       |
|    3     | Buffy    | Harold | dog     | f    | 1989-05-13 | NULL       |
|    4     | Fang     | Benny  | dog     | m    | 1990-08-27 | NULL       |
|    5     | Bowser   | Diane  | dog     | m    | 1979-08-31 | 1995-07-29 |
|    6     | Chirpy   | Gwen   | bird    | f    | 1998-09-11 | NULL       |
|    7     | Whistler | Gwen   | bird    | NULL | 1997-12-09 | NULL       |
|    8     | Slim     | Benny  | snake   | m    | 1996-04-29 | NULL       |
|    9     | Puffball | Diane  | hamster | f    | 1999-03-30 | NULL       |
Query: 
```sql
SELECT * FROM pets WHERE NOT species=’dog’;
```
Result:  
|    id    | name     | owner  | species | sex  | birth      | death      |
|----------|----------|--------|---------|------|------------|------------|
|    1     | Fluffy   | Harold | cat     | f    | 1993-02-04 | NULL       |
|    2     | Claws    | Gwen   | cat     | m    | 1994-03-17 | NULL       |
|    6     | Chirpy   | Gwen   | bird    | f    | 1998-09-11 | NULL       |
|    7     | Whistler | Gwen   | bird    | NULL | 1997-12-09 | NULL       |
|    8     | Slim     | Benny  | snake   | m    | 1996-04-29 | NULL       |
|    9     | Puffball | Diane  | hamster | f    | 1999-03-30 | NULL       |

## ORDER BY Keyword
The ORDER BY keyword is used to sort the result-set in ascending or descending order.  
The ORDER BY keyword sorts the records in ascending order by default. To sort the records in descending order, use the DESC keyword.  
Syntax:  
```sql
SELECT column1, column2, ...
FROM table_name
ORDER BY column1, column2, ... ASC|DESC;
```
Example: This is an example TABLE call pets.  
|    id    | name     | owner  | species | sex  | birth      | death      |
|----------|----------|--------|---------|------|------------|------------|
|    1     | Fluffy   | Harold | cat     | f    | 1993-02-04 | NULL       |
|    2     | Claws    | Gwen   | cat     | m    | 1994-03-17 | NULL       |
|    3     | Buffy    | Harold | dog     | f    | 1989-05-13 | NULL       |
|    4     | Fang     | Benny  | dog     | m    | 1990-08-27 | NULL       |
|    5     | Bowser   | Diane  | dog     | m    | 1979-08-31 | 1995-07-29 |
|    6     | Chirpy   | Gwen   | bird    | f    | 1998-09-11 | NULL       |
|    7     | Whistler | Gwen   | bird    | NULL | 1997-12-09 | NULL       |
|    8     | Slim     | Benny  | snake   | m    | 1996-04-29 | NULL       |
|    9     | Puffball | Diane  | hamster | f    | 1999-03-30 | NULL       |
Query: 
```sql
SELECT name, birth FROM pet ORDER BY birth ASC;
```
Result: 
| name     | birth      |
|----------|------------|
| Buffy    | 1989-05-13 |
| Bowser   | 1989-08-31 |
| Fang     | 1990-08-27 |
| Fluffy   | 1993-02-04 |
| Claws    | 1994-03-17 |
| Slim     | 1996-04-29 |
| Whistler | 1997-12-09 |
| Chirpy   | 1998-09-11 |
| Puffball | 1999-03-30 |

## INSERT INTO Statement
INSERT INTO is use to insert new record into database table.
Syntax:
```sql
INSERT INTO table_name (column1, column2, column3, ...)
VALUES (value1, value2, value3, ...);
```
Or

```sql
INSERT INTO table_name
VALUES (value1, value2, value3, ...);
```
Example: This is an example table call pets and we will insert new record to it.  
|    id    | name     | owner  | species | sex  | birth      | death      |
|----------|----------|--------|---------|------|------------|------------|
|    1     | Fluffy   | Harold | cat     | f    | 1993-02-04 | NULL       |
|    2     | Claws    | Gwen   | cat     | m    | 1994-03-17 | NULL       |
|    3     | Buffy    | Harold | dog     | f    | 1989-05-13 | NULL       |
|    4     | Fang     | Benny  | dog     | m    | 1990-08-27 | NULL       |
|    5     | Bowser   | Diane  | dog     | m    | 1979-08-31 | 1995-07-29 |
|    6     | Chirpy   | Gwen   | bird    | f    | 1998-09-11 | NULL       |
|    7     | Whistler | Gwen   | bird    | NULL | 1997-12-09 | NULL       |
|    8     | Slim     | Benny  | snake   | m    | 1996-04-29 | NULL       |
|    9     | Puffball | Diane  | hamster | f    | 1999-03-30 | NULL       |
Query: 
```sql
INSERT INTO pets (name, owner, species, sex, birth, death)
VALUES (‘Rainbow’, ‘John’, ‘bird’, ‘m’, ‘1999-04-10’, NULL);
```
Result: The TABLE will look like this  
|    id    | name     | owner  | species | sex  | birth      | death      |
|----------|----------|--------|---------|------|------------|------------|
|    1     | Fluffy   | Harold | cat     | f    | 1993-02-04 | NULL       |
|    2     | Claws    | Gwen   | cat     | m    | 1994-03-17 | NULL       |
|    3     | Buffy    | Harold | dog     | f    | 1989-05-13 | NULL       |
|    4     | Fang     | Benny  | dog     | m    | 1990-08-27 | NULL       |
|    5     | Bowser   | Diane  | dog     | m    | 1979-08-31 | 1995-07-29 |
|    6     | Chirpy   | Gwen   | bird    | f    | 1998-09-11 | NULL       |
|    7     | Whistler | Gwen   | bird    | NULL | 1997-12-09 | NULL       |
|    8     | Slim     | Benny  | snake   | m    | 1996-04-29 | NULL       |
|    9     | Puffball | Diane  | hamster | f    | 1999-03-30 | NULL       |
|    10    | Rainbow  | John   | bird    | m    | 1999-04-10 | NULL       |

## UPDATE Statement
UPDATE Statement is used to update existing record in the table.
Syntax:
```sql
UPDATE table_name
SET column1 = value1, column2 = value2, ...
WHERE condition;
```
Note: WHERE Clause is play very important role here, it will specify which record is going to be update, If WHERE Clause is not being use; all record in the TABLE will be updated.
Example: This is the example TABLE call pets and we will update a record on the table.
|    id    | name     | owner  | species | sex  | birth      | death      |
|----------|----------|--------|---------|------|------------|------------|
|    1     | Fluffy   | Harold | cat     | f    | 1993-02-04 | NULL       |
|    2     | Claws    | Gwen   | cat     | m    | 1994-03-17 | NULL       |
|    3     | Buffy    | Harold | dog     | f    | 1989-05-13 | NULL       |
|    4     | Fang     | Benny  | dog     | m    | 1990-08-27 | NULL       |
|    5     | Bowser   | Diane  | dog     | m    | 1979-08-31 | 1995-07-29 |
|    6     | Chirpy   | Gwen   | bird    | f    | 1998-09-11 | NULL       |
|    7     | Whistler | Gwen   | bird    | NULL | 1997-12-09 | NULL       |
|    8     | Slim     | Benny  | snake   | m    | 1996-04-29 | NULL       |
|    9     | Puffball | Diane  | hamster | f    | 1999-03-30 | NULL       |
|    10    | Rainbow  | John   | bird    | m    | 1999-04-10 | NULL       |  

Query: 
```sql
UPDATE pets SET owner=‘Dave’ WHERE id=10;
```
Result: The TABLE will look like this after UPDATE Statement ran.  

|    id    | name     | owner  | species | sex  | birth      | death      |
|----------|----------|--------|---------|------|------------|------------|
|    1     | Fluffy   | Harold | cat     | f    | 1993-02-04 | NULL       |
|    2     | Claws    | Gwen   | cat     | m    | 1994-03-17 | NULL       |
|    3     | Buffy    | Harold | dog     | f    | 1989-05-13 | NULL       |
|    4     | Fang     | Benny  | dog     | m    | 1990-08-27 | NULL       |
|    5     | Bowser   | Diane  | dog     | m    | 1979-08-31 | 1995-07-29 |
|    6     | Chirpy   | Gwen   | bird    | f    | 1998-09-11 | NULL       |
|    7     | Whistler | Gwen   | bird    | NULL | 1997-12-09 | NULL       |
|    8     | Slim     | Benny  | snake   | m    | 1996-04-29 | NULL       |
|    9     | Puffball | Diane  | hamster | f    | 1999-03-30 | NULL       |
|    10    | Rainbow  | Dave   | bird    | m    | 1999-04-10 | NULL       |

## DELETE Statement
DELETE Statement is used to delete record FROM database table.
Syntax:
```sql
DELETE FROM table_name WHERE condition;
```
Note: if you using DELETE Statement without WHERE Clause; all records in TABLE will be deleted.
Example: This is the example TABLE call pets and we will delete a record FROM the table.  

|    id    | name     | owner  | species | sex  | birth      | death      |
|----------|----------|--------|---------|------|------------|------------|
|    1     | Fluffy   | Harold | cat     | f    | 1993-02-04 | NULL       |
|    2     | Claws    | Gwen   | cat     | m    | 1994-03-17 | NULL       |
|    3     | Buffy    | Harold | dog     | f    | 1989-05-13 | NULL       |
|    4     | Fang     | Benny  | dog     | m    | 1990-08-27 | NULL       |
|    5     | Bowser   | Diane  | dog     | m    | 1979-08-31 | 1995-07-29 |
|    6     | Chirpy   | Gwen   | bird    | f    | 1998-09-11 | NULL       |
|    7     | Whistler | Gwen   | bird    | NULL | 1997-12-09 | NULL       |
|    8     | Slim     | Benny  | snake   | m    | 1996-04-29 | NULL       |
|    9     | Puffball | Diane  | hamster | f    | 1999-03-30 | NULL       |
|    10    | Rainbow  | Dave   | bird    | m    | 1999-04-10 | NULL       |  

Query: 
```sql
DELETE FROM pets WHERE death IS NOT NULL;
```
Note: It is not possible to use =, < or <> to check if a column is NULL or not. For this purposes we have to use IS NULL or IS NOT NULL Operator.
Result: The TABLE will look like this after DELETE Statement ran.  

|    id    | name     | owner  | species | sex  | birth      | death      |
|----------|----------|--------|---------|------|------------|------------|
|    1     | Fluffy   | Harold | cat     | f    | 1993-02-04 | NULL       |
|    2     | Claws    | Gwen   | cat     | m    | 1994-03-17 | NULL       |
|    3     | Buffy    | Harold | dog     | f    | 1989-05-13 | NULL       |
|    4     | Fang     | Benny  | dog     | m    | 1990-08-27 | NULL       |
|    6     | Chirpy   | Gwen   | bird    | f    | 1998-09-11 | NULL       |
|    7     | Whistler | Gwen   | bird    | NULL | 1997-12-09 | NULL       |
|    8     | Slim     | Benny  | snake   | m    | 1996-04-29 | NULL       |
|    9     | Puffball | Diane  | hamster | f    | 1999-03-30 | NULL       |
|    10    | Rainbow  | Dave   | bird    | m    | 1999-04-10 | NULL       |

## LIMIT Clause: LIMIT clause is the clause that we use to LIMIT the number of record that we want to SELECT.  
```sql
SELECT column_name(s)
FROM table_name
WHERE condition
LIMIT number;
```
Example: 
```sql
SELECT * FROM Customers
WHERE Country='Germany'
LIMIT 3;
```
## Keyword OFFSET: OFFSET Keyword is the way to cut or hide record above that we don’t want to see while using LIMIT.
```sql
SELECT * FROM Customers
LIMIT 10 OFFSET 5;
```
## MIN() and MAX()
The MIN() function returns the smallest value of the SELECTed column.  
```sql
SELECT MIN(column_name)
FROM table_name
WHERE condition;
```
Example: 
```sql
SELECT MIN(Price) AS SmallestPrice
FROM Products;
```
The MAX() function returns the largest value of the SELECTed column.  
```sql
SELECT MAX(column_name)
FROM table_name
WHERE condition;
```
Example:	
```sql
SELECT MAX(Price) AS LargestPrice
FROM Products;
```
## COUNT: COUNT() function returns the number of rows that matches a specified criterion.
```sql
SELECT COUNT(column_name)
FROM table_name
WHERE condition;
```
Example:
```sql
SELECT COUNT(ProductID)
FROM Products;
```
## AVG
AVG() function returns the average value of a numeric column.
Syntax:
```sql
SELECT AVG(column_name)
FROM table_name
WHERE condition;
```
Example:
```sql
SELECT AVG(Price)
FROM Products;
```
## SUM
SUM() function returns the total sum of a numeric column.
Syntax:
```sql
SELECT SUM(column_name)
FROM table_name
WHERE condition;
```
Example:
```sql
SELECT SUM(Quantity)
FROM OrderDetails;
```
## LIKE
LIKE operator is used in a WHERE clause  to search for a specified pattern in a column.
There are two wildcards often used in conjunction with the LIKE Operator:
- percent sign (%) represents zero, one, or multiple characters
- underscore sign (_) represents one, single character
Syntax:
```sql
SELECT column1, column2, ...
FROM table_name
WHERE columnN LIKE pattern;
```
Example:
```sql
SELECT * FROM Customers
WHERE CustomerName LIKE 'a%';
SELECT * FROM Customers
WHERE CustomerName LIKE '%a';
SELECT * FROM Customers
WHERE CustomerName LIKE '%or%';
```
## IN 
IN operator allows you to specify multiple values in a WHERE clause.
IN operator is a shorthand for multiple OR condition.
Sytax:
```sql
SELECT column_name(s)
FROM table_name
WHERE column_name IN (value1, value2, ...);
```
Example:
```sql
SELECT * FROM Customers
WHERE Country IN ('Germany', 'France', 'UK');
SELECT * FROM Customers
WHERE Country NOT IN ('Germany', 'France', 'UK’);
SELECT * FROM Customers
WHERE Country IN (SELECT Country FROM Suppliers);
```
## BETWEEN 
BETWEEN operator SELECTs values within a given range. The values can be numbers, text, or dates.
Syntax:
```sql
SELECT column_name(s)
FROM table_name
WHERE column_name BETWEEN value1 AND value2;
```
Example:
```sql
SELECT * FROM Orders
WHERE OrderDate BETWEEN '1996-07-01' AND '1996-07-31’;
SELECT * FROM Products
WHERE ProductName BETWEEN 'Carnarvon Tigers' AND 'Mozzarella di Giovanni'
ORDER BY ProductName;
```
## JOINNING TABLES
Join clause is used to combine rows FROM two or more tables, based on a related column BETWEEN them.
Types of Joins in MySQL:
### INNNER JOIN:
Returns records that have matching values in both tables.INNER JOIN keyword SELECTs records that have matching values in both tables.
Syntax:
```sql
SELECT column_name(s)
FROM table1
INNER JOIN table2
ON table1.column_name = table2.column_name;
```
Example:
```sql
SELECT ProductID, ProductName, CategoryName
FROM Products
INNER JOIN Categories ON Products.CategoryID = Categories.CategoryID;
```
### LEFT JOIN
Returns all records FROM the left table, and the matched records FROM the right table. BETWEENLEFT JOIN returns all records FROM the left TABLE (table1), and the matching records FROM the right TABLE (table2). The result is 0 records FROM the right side, if there is no match.
Syntax:
```sql
SELECT column_name(s)
FROM table1
LEFT JOIN table2
ON table1.column_name = table2.column_name;
```
Example:
```sql
SELECT Customers.CustomerName, Orders.OrderID
FROM Customers
LEFT JOIN Orders ON Customers.CustomerID = Orders.CustomerID
ORDER BY Customers.CustomerName;
```
### RIGHT JOIN
keyword returns all records FROM the right TABLE (table2), and the matching records (if any) FROM the left TABLE (table1).
Syntax:
```sql
SELECT column_name(s)
FROM table1
RIGHT JOIN table2 ON table1.column_name=table2.column_name;
```
Example:
```sql
SELECT Orders.OrderID, Employees.LastName, Employees.FirstName
FROM Orders
RIGHT JOIN Employees ON Orders.EmployeeID = Employees.EmployeeID
ORDER BY Orders.OrderID;
```
### CROSS JOIN
Returns all records FROM both tables
Syntax:
```sql
SELECT column_name(s)
FROM  table1
CROSS JOIN table2 ;
```
Example:
```sql
SELECT Customer.Orders.OrderID
FROM Customer
CROSS JOIN Orders;
```
### SELF JOIN 
is a regular, but the TABLE is joined with it self.
Syntax
```sql
SELECT column_name(s)
FROM  table1 t1,t2
WHERE Condition;
```
Example:
```sql
SELECT A.CustomerName AS CustomerName1, B.CustomerName AS CustomerName2, A.City
FROM Customers A, Customers B
WHERE A.CustomerID <> B.CustomerID
AND A.City = B.City 
ORDER BY A.City;
```
## UNION 
is a regular, but the TABLE is joined with itself.
Syntax:
```sql
SELECT column_name(s) FROM table1
UNION
SELECT column_name(s) FROM table2
```
Example:
```sql
SELECT City FROM Customers
UNION ALL
SELECT City FROM Suppliers
ORDER BY City;
```
## UNION ALL 
is a regular, but the TABLE is joined with itself.
Syntax:
```sql
SELECT column_name(s) FROM table1
UNION ALL 
SELECT column_name(s) FROM table2
```
Example:
```sql
SELECT City FROM Customers
UNION ALL
SELECT City FROM Suppliers
ORDER BY City;
```
## GROUP BY 
group rows that have the same value into summary row, lile ‘find the number of customer in each country’.
Syntax:
```sql
SELECT column_name(s) FROM table1_name
WHERE condition
GROUP BY column_name(s)
```
Example:
```sql
SELECT COUNT(CustomerID), Country
FROM Customers
GROUP BY Country;
```
## HAVING 
claus was added to SQL because the WHERE the keyword cannot be used with aggregate function.
Syntax:
```sql
SELECT column_name(s)
FROM table_name
WHERE condition
GROUP BY column_name(s)
HAVING condition
ORDER BY column_name(s);
```
Example:
```sql
SELECT COUNT(CustomerID), Country
FROM Customers
GROUP BY Country
HAVING COUNT(CustomerID) > 5;
```
## EXISTS
operator is used to test for the extansince of any record in a subquery. Operator return TRUE if subquery return one or more records.
Syntax:
```sql
SELECT column_name(s) FROM table_name
WHERE EXISTS (SELECT column_name 
FROM table_name WHERE condition);
```
Example:
```sql
SELECT SupplierName
FROM Suppliers
WHERE EXISTS (SELECT ProductName FROM Products WHERE Products.SupplierID = Suppliers.supplierID AND Price = 22);
```
## ANY, ALL
are operator are allow you perform a comparison BETWEEN a single value and range of others value. Operator ANY:
-Return a boolean value as a result.
-Return true if ANY of the subquery value meet the condition.
Syntax:
```sql
SELECT column_name(s)FROM table_name
WHERE column_name operator 
ANY (SELECT column_name
FROM table_name  WHERE condition);
```
Example:
```sql
SELECT ProductName
FROM Products
WHERE ProductID = ANY (SELECT ProductID
FROM OrderDetails 
WHERE Quantity = 10);
```
## ANY, ALL(con.)
Syntax:
```sql
SELECT column_name(s) FROM table_name
WHERE column_name operator ALL
(SELECT column_name FROM table_name WHERE condition);
```
Example:
```sql
SELECT ProductName FROM Products
WHERE ProductID = ALL(SELECT ProductID
FROM OrderDetails WHERE Quantity = 10);
```
## INSERT SELECT
-Insert SELECT: statement copies data FROM one TABLE and inserts into another table.
-Insert SELECT: statement required that the data type in source and target TABLE match.
Syntax:
```sql
INSERT INTO table2 (column1, column2, ...)
SELECT column1, column2, ...FROM table1
WHERE condition;
```
Example:
```sql
INSERT INTO Customers (CustomerName,City,Country)
SELECT SupplierName, City, Country FROM Suppliers
WHERE Country='Germany';
```
## CASE
Statement goes through condition and return a value when the first condition is meet(like an if-then-else statement).
once a condition is true, it will stop reading and return the result. 
If no conditions are true, it returns the value in the ELSE clause.
If there are no ELSE part and no condition are true, it return NULL.
Syntax:
```sql
CASE
    WHEN condition1 THEN result1
    WHEN condition2 THEN result2
    WHEN conditionN THEN resultN
    ELSE result
END;
```
Example:
```sql
SELECT CustomerName, City, Country
FROM Customers
ORDER BY
(CASE
    WHEN City IS NULL THEN Country
    ELSE City
END);  
```
## CREATE Database
statement is used to CREATE a new SQL database
Syntax:
```sql
CREATE database db_name;
```
example: 
```sql
CREATE database shop;
```
## Drop Database 
statement is used to drop an existing SQL database
Syntax:
```sql
drop database db_name;
```
Example: 
```sql
drop database shop;
```
## CREATE Table
statement is used to CREATE a new TABLE in a database
Syntax:
```sql
CREATE TABLE name_tb(
	column1 datatype,
    column2 datatype,
    …..
);
```
Example:
```sql
CREATE TABLE drink(
Name	varchar(),
Price	float,
);
```
## Drop TABLE 
statement is used to drop an existing TABLE in a database
Syntax:
```sql
DROP TABLE table_name;
```
Example: 
```sql
drop TABLE drink;
```
## Rename table
statement is used to rename an TABLE in a database
Syntax:
```sql
Rename TABLE old_tb to new_tb;
```
Example: 
```sql
rename TABLE drink to food;
```
## Alter table
### ALTER TABLE add
use to add a column in a table
Syntax:
```sql
ALTER TABLE tb_name add column datatype;
```
Example: 
```sql
alter TABLE drink add beer varchar(20);
```
### ALTER TABLE rename
use to rename a column in table
Syntax:
```sql
ALTER TABLE tb_name rename column1 TO column2;
```
Example: 
```sql
ALTER TABLE drink rename column beer TO milk_tea;
```
### ALTER DROP column
use to delete a column in a table
Syntax:
```sql
ALTER TABLE table_name DROP COLUMN column_name;
```
Example: 
```sql
ALTER TABLE drink DROP COLUMN beer;
```
### ALTER MODIFY 
use to change the data type of a column in a table
Syntax:
```sql
ALTER TABLE tb_name
MODIFY COLUMN column datatype;
```
Example:
```sql
ALTER TABLE drink
MODIFY COLUMN beer varchar(300);
```
### ALTER MOVE
use to move the column in the table
Syntax:
```sql
ALTER TABLE tb_name MODIFY column1 AFTER column2;
```
Example: 
```SQL
ALTER TABLE drink MODIFY milk_tea AFTER beer;
```
## Not NULL : is constraint that won’t a column to NULL values.
```sql
CREATE TABLE Persons (
   Column1 datatype not NULL,
 Column2 datatype not NULL,
………… 
);
```
Example: 
```sql
CREATE TABLE person(
    ID int NOT NULL,
    LastName varchar(255) NOT NULL,
    FirstName varchar(255) NOT NULL,
    Age int
);
```
## Unique
UNIQUE constraint ensures that all values in a column are different.
Syntax:
```sql
CREATE TABLE Persons (
   Column1 datatype,
 Column2 datatype unique,
……
);
```
Example:
```sql
CREATE TABLE Persons (
    ID int,
    LastName varchar(255) ,
    FirstName varchar(255),
    Age int,
    UNIQUE (ID)
);
```
## PRIMARY key
The PRIMARY key can only one PRIMARY key per TABLE and must contain UNIQUE values and cannot contain NULL values
```sql
CREATE TABLE Persons (
    column datatype ,
    column datatype,
    …
    PRIMARY KEY (column)
);
```
Example:
```sql
CREATE TABLE person(
    ID int ,
    LastName varchar(255),
    FirstName varchar(255),
    Age int,
    PRIMARY KEY (ID)
);
```
## Foreing key: is have a link BETWEEN one TABLE with another table;
### Student table
```sql
CREATE TABLE Student (
    id int ,
    namestudent varchar(200),
    Score int,
);
```
### Employee table
```sql
CREATE TABLE Employee(
    id int ,
    nameemployee varchar(200),
    salary int,
);
```
### transaction table
```sql
CREATE TABLE transaction(
    dayoff int ,
    transaction_id int, 
    id int ,
    foreign key(id) references employee(id)
);
```
## CHECK
The CHECK constraint is used to LIMIT the value range that can be placed in a column.
```sql
CREATE TABLE Persons (
    ID int NOT NULL,
    LastName varchar(255) NOT NULL,
    FirstName varchar(255),
    Age int,
    CHECK (Age>=18)
);
```
## DEFAULT
The DEFAULT constraint is used to set a default value for a column.
The default value will be added to all new records, if no other value is specified.
```sql
CREATE TABLE Persons (
    ID int NOT NULL,
    LastName varchar(255) NOT NULL,
    FirstName varchar(255),
    Age int,
    City varchar(255) DEFAULT 'Sandnes'
);
```
## CREATE INDEX
The CREATE INDEX statement is used to CREATE indexes in tables.
Indexes are used to retrieve data FROM the database more quickly than otherwise. The users cannot see the indexes, they are just used to speed up searches/queries.
```sql
CREATE INDEX index_name
ON table_name (column1, column2, ...);
CREATE UNIQUE INDEX index_name
ON table_name (column1, column2, ...);
```
## AUTO INCREMENT
Auto-increment allows a unique number to be generated automatically when a new record is inserted into a table.
```sql
CREATE TABLE Persons (
    Personid int NOT NULL AUTO_INCREMENT,
    LastName varchar(255) NOT NULL,
    FirstName varchar(255),
    Age int,
    PRIMARY KEY (Personid)
);
```
## DATES
MySQL comes with the following data types for storing a date or a date/time value in the database:
•	DATE - format YYYY-MM-DD
•	DATETIME - format: YYYY-MM-DD HH:MI:SS
•	TIMESTAMP - format: YYYY-MM-DD HH:MI:SS
•	YEAR - format YYYY or YY		
Note: The date data type are set for a column when you CREATE a new TABLE in your database!
```sql
SELECT * FROM Orders WHERE OrderDate='2008-11-11';
```
## VIEW
In SQL, a view is a virtual TABLE based on the result-set of an SQL statement.
A view is created with the CREATE VIEW statement.
Syntax:
```sql
CREATE VIEW view_name AS
SELECT column1, column2, ...
FROM table_name
WHERE condition;
```
Example:
```sql
CREATE VIEW [Brazil Customers] AS
SELECT CustomerName, ContactName
FROM Customers
WHERE Country = 'Brazil';
```

## We can query the view above as follows:
``` sql
SELECT * FROM [Brazil Customers];
```

Reference:
- https://www.w3schools.com/mysql/default.asp
- https://www.oracle.com/mysql/what-is-mysql/ 
