# Basic MySQL  
## Overview  
This repository houses the Backend Group 1 Showcase Project, created to demonstrate the application of basic MySQL skills. The project serves as a practical example of what the team has learned in Basic MySQL course at Kilo IT backend skill training.  
## Authors  
1. [Nam Phathyuth](https://gitlab.com/Nam-Phathyuth)
2. [Yet Pheasa](https://gitlab.com/pheasadeveloper)
3. [Say Dane](https://gitlab.com/danejoker1113)
4. [Heng Sokly](https://gitlab.com/soklyh6219)
5. [Sim Viseth](https://gitlab.com/simviseth)  
## [Project Documentation](mysql.md)
## [Download Project Pdf](mysql.pdf)